function convertJSONtoTable(jsonData, divElement) {
  var cols = [];

  for (var i = 0; i < jsonData.length; i++) {
    for (var k in jsonData[i]) {
      if (cols.indexOf(k) === -1) {
        // Push all keys to the array
        cols.push(k);
      }
    }
  }

  // Create a table element
  var table = document.createElement("table");
  table.setAttribute("id", divElement + "innerTableId");
  table.setAttribute("class", "table table-striped table-bordered");
  table.setAttribute("style", "background-color: white; width: 100%; table-layout: auto;");

  // Create table row tr element of a table

  var thead = table.createTHead();
  var theadtr = thead.insertRow(-1);

  for (var i = 0; i < cols.length; i++) {
    // Create the table header th element
    var label = document.createElement("th");
    label.innerHTML = cols[i];

    // Append columnName to the table row
    theadtr.appendChild(label);
  }

  var tbody = table.createTBody();

  // Adding the data to the table
  for (var i = 0; i < jsonData.length; i++) {
    // Create a new row
    trow = tbody.insertRow(-1);
    for (var j = 0; j < cols.length; j++) {
      var cell = trow.insertCell(-1);

      // Inserting the cell at particular place
      cell.innerHTML = jsonData[i][cols[j]];
    }
  }

  // Add the newely created table containing json data
  var el = document.getElementById(divElement);
  el.innerHTML = "";
  el.appendChild(table);
}
