function toggleOverlay() {
        var element = document.getElementById("overlay"),
          style = window.getComputedStyle(element),
          displayMode = style.getPropertyValue("display");

        if (displayMode === "none") {
          document.getElementById("overlay").style.display = "block";
        } else {
          document.getElementById("overlay").style.display = "none";
        }
      }