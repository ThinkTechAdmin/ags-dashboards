/////// PIE STARTS HERE

      var randomScalingFactor = function () {
        return Math.round(Math.random() * 100);
      };

      var configPie = {
        type: "pie",
        data: {
          datasets: [
            
          ],
          //labels: ["Red", "Orange", "Yellow", "Green", "Blue"],
        },
        options: {
          responsive: true,
          aspectRatio: 1,
          legend: {
          	display: false
          },
          title: {
          	display: true,
          	text: "Customer Inv Count"
          }
        },
      };

      ////// BAR STARTS HERE

      var color = Chart.helpers.color;
      var barChartData = {        
        datasets: [],
      };

      var barConfig = {
        type: "bar",
        data: barChartData,
        options: {
          responsive: true,
          aspectRatio: 1,
          legend: {
            display: false
          },
          title: {
            display: true,
            text: "Cust Status Count",
          },
        },
      };

      //// LOAD GRAPHS

      window.onload = function () {
        var ctxBar = document.getElementById("barCanvas").getContext("2d");
        window.myBar = new Chart(ctxBar, barConfig);

        var ctxPie = document.getElementById("pieCanvas").getContext("2d");
        window.myPie = new Chart(ctxPie, configPie);
      };