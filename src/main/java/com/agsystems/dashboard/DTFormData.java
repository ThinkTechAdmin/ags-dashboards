package com.agsystems.dashboard;

public class DTFormData
{
    String fromDate;
    String toDate;
    String handling;
    Integer house;
    Integer suffix;
    String rebillYN;
    String billTo;
    String location;
    String service;
    String station;
    Integer vendor;
    Integer ediid;
    
    public Integer getVendor() {
        return this.vendor;
    }
    
    public void setVendor(final Integer vendor) {
        this.vendor = vendor;
    }
    
    public Integer getEdiId() {
        return this.vendor;
    }
    
    public void setEdiId(final Integer ediid) {
        this.ediid = ediid;
    }
    
    public String getHandling() {
        return this.handling;
    }
    
    public void setHandling(final String handling) {
        this.handling = handling;
    }
    
    public String getLocation() {
        return this.location;
    }
    
    public void setLocation(final String location) {
        this.location = location;
    }
    
    public Integer getHouse() {
        return this.house;
    }
    
    public void setHouse(final Integer house) {
        this.house = house;
    }
    
    public Integer getSuffix() {
        return this.suffix;
    }
    
    public void setSuffix(final Integer suffix) {
        this.suffix = suffix;
    }
    
    public String getRebillYN() {
        return this.rebillYN;
    }
    
    public void setRebillYN(final String rebillYN) {
        this.rebillYN = rebillYN;
    }
    
    public String getBillTo() {
        return this.fromDate;
    }
    
    public void setBillTo(final String billTo) {
        this.billTo = billTo;
    }
    
    public String getFromDate() {
        return this.fromDate;
    }
    
    public void setFromDate(final String fromDate) {
        this.fromDate = fromDate;
    }
    
    public String getToDate() {
        return this.toDate;
    }
    
    public void setToDate(final String toDate) {
        this.toDate = toDate;
    }
    
    public String getService() {
        return this.service;
    }
    
    public void setService(final String service) {
        this.service = service;
    }
    
    public String getStation() {
        return this.station;
    }
    
    public void setStation(final String station) {
        this.station = station;
    }
}
