package com.agsystems.dashboard;

import org.springframework.web.server.ResponseStatusException;
import java.time.temporal.TemporalUnit;
import java.time.temporal.ChronoUnit;
import java.time.Instant;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PostMapping;
import java.util.Date;
import java.text.DateFormat;
import org.json.JSONArray;
import org.springframework.http.HttpStatus;
import java.text.ParseException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping({ "v1/dashboards" })
@RestController
public class InvoiceStatusController
{
    @PostMapping(path = { "vendorEDIMatchByDateByVendor" }, consumes = { "application/x-www-form-urlencoded", "multipart/form-data" }, produces = { "application/json" })
    @ResponseBody
    public ResponseEntity<List<Object>> vendorEDIMatchByDateByVendor(final DTFormData formData) {
        final DBUtils db = new DBUtils();
        JSONArray results = null;
        try {
            final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            final Date date = formatter.parse(formData.getFromDate());
            final Timestamp fromDT = new Timestamp(date.getTime());
            final Date date2 = formatter.parse(formData.getToDate());
            final Timestamp toDT = new Timestamp(date2.getTime());
            System.out.println(fromDT + " - " + toDT);
            results = db.getTable("call AGUNIVERSE.RPT_VENDOREDIMATCHBYDATEBYVENDOR(?,?)", fromDT, toDT);
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        catch (ParseException e2) {
            e2.printStackTrace();
        }
        return (ResponseEntity<List<Object>>)new ResponseEntity((Object)results.toList(), HttpStatus.OK);
    }
    
    @RequestMapping({ "vendorEDIByDateByVendor" })
    @ResponseBody
    public ResponseEntity<List<Object>> vendorEDIByDateByVendor() {
        final DBUtils db = new DBUtils();
        JSONArray results = null;
        try {
            final Timestamp fromDT = Timestamp.from(Instant.now().minus(24L, (TemporalUnit)ChronoUnit.HOURS));
            final Timestamp toDT = Timestamp.from(Instant.now());
            results = db.getTable("call call AGUNIVERSE.RPT_VENDOREDIBYDATEBYVENDOR(?,?)", fromDT, toDT);
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return (ResponseEntity<List<Object>>)new ResponseEntity((Object)results.toList(), HttpStatus.OK);
    }
    
    @RequestMapping(path = { "ediCustInvoiceCountAmountByDate" }, consumes = { "application/x-www-form-urlencoded", "multipart/form-data" }, produces = { "application/json" })
    @ResponseBody
    public ResponseEntity<List<Object>> ediCustInvoiceCountAmountByDate(final DTFormData formData) {
        final DBUtils db = new DBUtils();
        JSONArray results = null;
        try {
            final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            final Date date = formatter.parse(formData.getFromDate());
            final Timestamp fromDT = new Timestamp(date.getTime());
            final Date date2 = formatter.parse(formData.getToDate());
            final Timestamp toDT = new Timestamp(date2.getTime());
            results = db.getTable("call AGUNIVERSE.RPT_EDICUSTINVOICECOUNTAMOUNTBYDATE(?,?)", fromDT, toDT);
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid data", (Throwable)e);
        }
        catch (ParseException e2) {
            e2.printStackTrace();
        }
        return (ResponseEntity<List<Object>>)new ResponseEntity((Object)results.toList(), HttpStatus.OK);
    }
    
    @RequestMapping(path = { "invoiceStatusTableAndChart" }, consumes = { "application/x-www-form-urlencoded", "multipart/form-data" }, produces = { "application/json" })
    @ResponseBody
    public ResponseEntity<List<Object>> invoiceStatus(final DTFormData formData) {
        final DBUtils db = new DBUtils();
        JSONArray results = null;
        try {
            final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            final Date date = formatter.parse(formData.getFromDate());
            final Timestamp fromDT = new Timestamp(date.getTime());
            final Date date2 = formatter.parse(formData.getToDate());
            final Timestamp toDT = new Timestamp(date2.getTime());
            results = db.getTable("call AGUNIVERSE.RPT_EDICUSTSTATUSCOUNTBYDATE(?,?)", fromDT, toDT);
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid data", (Throwable)e);
        }
        catch (ParseException e2) {
            e2.printStackTrace();
        }
        return (ResponseEntity<List<Object>>)new ResponseEntity((Object)results.toList(), HttpStatus.OK);
    }
    
    @RequestMapping({ "selectedInvoicedEDICustomer" })
    @ResponseBody
    public ResponseEntity<List<Object>> selectedInvoicedEdiCustomers() {
        final DBUtils db = new DBUtils();
        JSONArray results = null;
        try {
            results = db.getTable("call AGUNIVERSE.SP_SELECTINVOICEEDICUSTOMERS()");
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return (ResponseEntity<List<Object>>)new ResponseEntity((Object)results.toList(), HttpStatus.OK);
    }
    
    @RequestMapping({ "selectedStatusCustomers" })
    @ResponseBody
    public ResponseEntity<List<Object>> selectedStatusCustomers() {
        final DBUtils db = new DBUtils();
        JSONArray results = null;
        try {
            results = db.getTable("call AGUNIVERSE.SP_SELECTSTATUSEDICUSTOMERS()");
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return (ResponseEntity<List<Object>>)new ResponseEntity((Object)results.toList(), HttpStatus.OK);
    }
    
    @RequestMapping({ "selectStations" })
    @ResponseBody
    public ResponseEntity<List<Object>> selectStations() {
        final DBUtils db = new DBUtils();
        JSONArray results = null;
        try {
            results = db.getTable("call AGUNIVERSE.SP_SELECTSTATIONS()");
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return (ResponseEntity<List<Object>>)new ResponseEntity((Object)results.toList(), HttpStatus.OK);
    }
    
    @RequestMapping({ "selectedCustomerStatus" })
    @ResponseBody
    public ResponseEntity<List<Object>> customerStatus() {
        final DBUtils db = new DBUtils();
        JSONArray results = null;
        try {
            final int ediID = 133;
            results = db.getTable("call AGUNIVERSE.SP_SELECTEDICUSTOMERSTATUSBYEDIID(?)", ediID);
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return (ResponseEntity<List<Object>>)new ResponseEntity((Object)results.toList(), HttpStatus.OK);
    }
    
    @RequestMapping({ "selectReportByLocation" })
    @ResponseBody
    public ResponseEntity<List<Object>> selectReportByLocation() {
        final DBUtils db = new DBUtils();
        JSONArray results = null;
        try {
            final String loc = "EDI";
            results = db.getTable("call AGUNIVERSE.SP_SELECTREPORTSBYLOC(?)", loc);
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return (ResponseEntity<List<Object>>)new ResponseEntity((Object)results.toList(), HttpStatus.OK);
    }
    
    @PostMapping(path = { "shipmentsByServiceByDateByLOC" }, consumes = { "application/x-www-form-urlencoded", "multipart/form-data" }, produces = { "application/json" })
    @ResponseBody
    public ResponseEntity<List<Object>> shipmentsByServiceByDateByLOC(final DTFormData formData) {
        final DBUtils db = new DBUtils();
        JSONArray results = null;
        try {
            final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            final Date date = formatter.parse(formData.getFromDate());
            final Timestamp fromDT = new Timestamp(date.getTime());
            final Date date2 = formatter.parse(formData.getToDate());
            final Timestamp toDT = new Timestamp(date2.getTime());
            final int fromDTInt = Integer.parseInt(fromDT.toString().split(" ")[0].replace("-", ""));
            final int toDTInt = Integer.parseInt(toDT.toString().split(" ")[0].replace("-", ""));
            final String location = formData.getLocation();
            System.out.println(fromDT + " - " + toDT);
            results = db.getTable("call AGUNIVERSE.RPT_shipmentsByServiceByDateByLOC(?,?,?)", fromDTInt, toDTInt, location);
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        catch (ParseException e2) {
            e2.printStackTrace();
        }
        return (ResponseEntity<List<Object>>)new ResponseEntity((Object)results.toList(), HttpStatus.OK);
    }
    
    @PostMapping(path = { "salesT1SHIPMENTSBYSTATUSBYLOC" }, consumes = { "application/x-www-form-urlencoded", "multipart/form-data" }, produces = { "application/json" })
    @ResponseBody
    public ResponseEntity<List<Object>> salesT1SHIPMENTSBYSTATUSBYLOC(final DTFormData formData) {
        final DBUtils db = new DBUtils();
        JSONArray results = null;
        try {
            final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            final Date date = formatter.parse(formData.getFromDate());
            final Timestamp fromDT = new Timestamp(date.getTime());
            final Date date2 = formatter.parse(formData.getToDate());
            final Timestamp toDT = new Timestamp(date2.getTime());
            final String location = formData.getLocation();
            System.out.println(fromDT + " - " + toDT);
            results = db.getTable("call AGUNIVERSE.RPT_SHIPMENTSBYSTATUSBYLOC(?)", location);
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        catch (ParseException e2) {
            e2.printStackTrace();
        }
        return (ResponseEntity<List<Object>>)new ResponseEntity((Object)results.toList(), HttpStatus.OK);
    }
    
    @PostMapping(path = { "salesT2SHIPMENTSBYSALESCODEBYDATEBYLOC" }, consumes = { "application/x-www-form-urlencoded", "multipart/form-data" }, produces = { "application/json" })
    @ResponseBody
    public ResponseEntity<List<Object>> salesT2SHIPMENTSBYSALESCODEBYDATEBYLOC(final DTFormData formData) {
        final DBUtils db = new DBUtils();
        JSONArray results = null;
        try {
            final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            final Date date = formatter.parse(formData.getFromDate());
            final Timestamp fromDT = new Timestamp(date.getTime());
            final Date date2 = formatter.parse(formData.getToDate());
            final Timestamp toDT = new Timestamp(date2.getTime());
            System.out.println(fromDT.toString().split(" ")[0]);
            final int fromDTInt = Integer.parseInt(fromDT.toString().split(" ")[0].replace("-", ""));
            final int toDTInt = Integer.parseInt(toDT.toString().split(" ")[0].replace("-", ""));
            final String location = formData.getLocation();
            System.out.println(fromDT + " - " + toDT);
            results = db.getTable("call AGUNIVERSE.RPT_SHIPMENTSBYSALESCODEBYDATEBYLOC(?,?,?)", fromDTInt, toDTInt, location);
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        catch (ParseException e2) {
            e2.printStackTrace();
        }
        return (ResponseEntity<List<Object>>)new ResponseEntity((Object)results.toList(), HttpStatus.OK);
    }
    
    @PostMapping(path = { "shipmentsSLATEONTIMEBYLOC" }, consumes = { "application/x-www-form-urlencoded", "multipart/form-data" }, produces = { "application/json" })
    @ResponseBody
    public ResponseEntity<List<Object>> shipmentsSLATEONTIMEBYLOC(final DTFormData formData) {
        final DBUtils db = new DBUtils();
        JSONArray results = null;
        try {
            final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            final Date date = formatter.parse(formData.getFromDate());
            final Timestamp fromDT = new Timestamp(date.getTime());
            final Date date2 = formatter.parse(formData.getToDate());
            final Timestamp toDT = new Timestamp(date2.getTime());
            System.out.println(fromDT.toString().split(" ")[0]);
            final int fromDTInt = Integer.parseInt(fromDT.toString().split(" ")[0].replace("-", ""));
            final int toDTInt = Integer.parseInt(toDT.toString().split(" ")[0].replace("-", ""));
            final String location = formData.getLocation();
            System.out.println(fromDT + " - " + toDT);
            results = db.getTable("call AGUNIVERSE.RPT_SHIPMENTSLATEONTIMEBYLOC(?,?,?)", fromDTInt, toDTInt, location);
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        catch (ParseException e2) {
            e2.printStackTrace();
        }
        return (ResponseEntity<List<Object>>)new ResponseEntity((Object)results.toList(), HttpStatus.OK);
    }
    
    @PostMapping(path = { "numberOfShipmentsPerServiceLevel" }, consumes = { "application/x-www-form-urlencoded", "multipart/form-data" }, produces = { "application/json" })
    @ResponseBody
    public ResponseEntity<List<Object>> numberOfShipmentsPerServiceLevel(final DTFormData formData) {
        final DBUtils db = new DBUtils();
        JSONArray results = null;
        try {
            final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            final Date date = formatter.parse(formData.getFromDate());
            final Timestamp fromDT = new Timestamp(date.getTime());
            final Date date2 = formatter.parse(formData.getToDate());
            final Timestamp toDT = new Timestamp(date2.getTime());
            System.out.println(fromDT.toString().split(" ")[0]);
            final int fromDTInt = Integer.parseInt(fromDT.toString().split(" ")[0].replace("-", ""));
            final int toDTInt = Integer.parseInt(toDT.toString().split(" ")[0].replace("-", ""));
            final String location = formData.getLocation();
            System.out.println(fromDT + " - " + toDT);
            results = db.getTable("call AGUNIVERSE.RPT_SHIPMENTSBYSERVICEBYDATEBYLOC(?,?,?)", fromDTInt, toDTInt, location);
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        catch (ParseException e2) {
            e2.printStackTrace();
        }
        return (ResponseEntity<List<Object>>)new ResponseEntity((Object)results.toList(), HttpStatus.OK);
    }
    
    @PostMapping(path = { "revenuePerBillTo" }, consumes = { "application/x-www-form-urlencoded", "multipart/form-data" }, produces = { "application/json" })
    @ResponseBody
    public ResponseEntity<List<Object>> revenuePerBillTo(final DTFormData formData) {
        final DBUtils db = new DBUtils();
        JSONArray results = null;
        try {
            final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            final Date date = formatter.parse(formData.getFromDate());
            final Timestamp fromDT = new Timestamp(date.getTime());
            final Date date2 = formatter.parse(formData.getToDate());
            final Timestamp toDT = new Timestamp(date2.getTime());
            System.out.println(fromDT.toString().split(" ")[0]);
            final int fromDTInt = Integer.parseInt(fromDT.toString().split(" ")[0].replace("-", ""));
            final int toDTInt = Integer.parseInt(toDT.toString().split(" ")[0].replace("-", ""));
            final String station = "ETK";
            System.out.println(fromDT + " - " + toDT + formData.getLocation());
            results = db.getTable("call AGUNIVERSE.RPT_SELECTAUDITBYGP(?,?,?)", fromDTInt, toDTInt, station);
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        catch (ParseException e2) {
            e2.printStackTrace();
        }
        return (ResponseEntity<List<Object>>)new ResponseEntity((Object)results.toList(), HttpStatus.OK);
    }
    
    @PostMapping(path = { "revenuePerBillToLoc" }, consumes = { "application/x-www-form-urlencoded", "multipart/form-data" }, produces = { "application/json" })
    @ResponseBody
    public ResponseEntity<List<Object>> revenuePerBillToLoc(final DTFormData formData) {
        final DBUtils db = new DBUtils();
        JSONArray results = null;
        try {
            final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            final Date date = formatter.parse(formData.getFromDate());
            final Timestamp fromDT = new Timestamp(date.getTime());
            final Date date2 = formatter.parse(formData.getToDate());
            final Timestamp toDT = new Timestamp(date2.getTime());
            System.out.println(fromDT.toString().split(" ")[0]);
            final int fromDTInt = Integer.parseInt(fromDT.toString().split(" ")[0].replace("-", ""));
            final int toDTInt = Integer.parseInt(toDT.toString().split(" ")[0].replace("-", ""));
            final String location = formData.getLocation();
            System.out.println(fromDT + " - " + toDT + formData.getLocation());
            results = db.getTable("call AGUNIVERSE.RPT_SELECTAUDITBYGP(?,?,?)", fromDTInt, toDTInt, location);
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        catch (ParseException e2) {
            e2.printStackTrace();
        }
        return (ResponseEntity<List<Object>>)new ResponseEntity((Object)results.toList(), HttpStatus.OK);
    }
    
    @PostMapping(path = { "invoicedActivityPerBillTo" }, consumes = { "application/x-www-form-urlencoded", "multipart/form-data" }, produces = { "application/json" })
    @ResponseBody
    public ResponseEntity<List<Object>> invoicedActivityPerBillTo(final DTFormData formData) {
        final DBUtils db = new DBUtils();
        JSONArray results = null;
        try {
            final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            final Date date = formatter.parse(formData.getFromDate());
            final Timestamp fromDT = new Timestamp(date.getTime());
            final Date date2 = formatter.parse(formData.getToDate());
            final Timestamp toDT = new Timestamp(date2.getTime());
            System.out.println(fromDT.toString().split(" ")[0]);
            final int fromDTInt = Integer.parseInt(fromDT.toString().split(" ")[0].replace("-", ""));
            final int toDTInt = Integer.parseInt(toDT.toString().split(" ")[0].replace("-", ""));
            final String station = "ETK";
            System.out.println(fromDT + " - " + toDT);
            results = db.getTable("call AGUNIVERSE.RPT_SELECTAUDITBYREV(?,?,?)", fromDTInt, toDTInt, station);
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        catch (ParseException e2) {
            e2.printStackTrace();
        }
        return (ResponseEntity<List<Object>>)new ResponseEntity((Object)results.toList(), HttpStatus.OK);
    }
    
    @PostMapping(path = { "invoicedActivityPerBillToLoc" }, consumes = { "application/x-www-form-urlencoded", "multipart/form-data" }, produces = { "application/json" })
    @ResponseBody
    public ResponseEntity<List<Object>> invoicedActivityPerBillToLoc(final DTFormData formData) {
        final DBUtils db = new DBUtils();
        JSONArray results = null;
        try {
            final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            final Date date = formatter.parse(formData.getFromDate());
            final Timestamp fromDT = new Timestamp(date.getTime());
            final Date date2 = formatter.parse(formData.getToDate());
            final Timestamp toDT = new Timestamp(date2.getTime());
            System.out.println(fromDT.toString().split(" ")[0]);
            final int fromDTInt = Integer.parseInt(fromDT.toString().split(" ")[0].replace("-", ""));
            final int toDTInt = Integer.parseInt(toDT.toString().split(" ")[0].replace("-", ""));
            final String station = "ETK";
            System.out.println(fromDT + " - " + toDT);
            results = db.getTable("call AGUNIVERSE.RPT_SELECTAUDITBYREV(?,?,?)", fromDTInt, toDTInt, station);
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        catch (ParseException e2) {
            e2.printStackTrace();
        }
        return (ResponseEntity<List<Object>>)new ResponseEntity((Object)results.toList(), HttpStatus.OK);
    }
    
    @RequestMapping({ "salesReportList" })
    @ResponseBody
    public ResponseEntity<List<Object>> salesReportList() {
        final DBUtils db = new DBUtils();
        JSONArray results = null;
        try {
            final String loc = "ETK";
            results = db.getTable("call AGUNIVERSE.SP_SELECTREPORTSBYLOC(?)", loc);
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return (ResponseEntity<List<Object>>)new ResponseEntity((Object)results.toList(), HttpStatus.OK);
    }
    
    @PostMapping(path = { "RPT_EDICUSTINVOICECOUNTAMOUNTBYDATE" }, consumes = { "application/x-www-form-urlencoded", "multipart/form-data" }, produces = { "application/json" })
    @ResponseBody
    public ResponseEntity<List<Object>> RPT_EDICUSTINVOICECOUNTAMOUNTBYDATE(final DTFormData formData) {
        final DBUtils db = new DBUtils();
        JSONArray results = null;
        try {
            final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            final Date date = formatter.parse(formData.getFromDate());
            final Timestamp fromDT = new Timestamp(date.getTime());
            final Date date2 = formatter.parse(formData.getToDate());
            final Timestamp toDT = new Timestamp(date2.getTime());
            results = db.getTable("CALL AGUNIVERSE.RPT_EDICUSTINVOICECOUNTAMOUNTBYDATE(?,?)", fromDT, toDT);
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        catch (ParseException e2) {
            e2.printStackTrace();
        }
        return (ResponseEntity<List<Object>>)new ResponseEntity((Object)results.toList(), HttpStatus.OK);
    }
    
    @PostMapping(path = { "RPT_EDICUSTSTATUSCOUNTBYDATE" }, consumes = { "application/x-www-form-urlencoded", "multipart/form-data" }, produces = { "application/json" })
    @ResponseBody
    public ResponseEntity<List<Object>> RPT_EDICUSTSTATUSCOUNTBYDATE(final DTFormData formData) {
        final DBUtils db = new DBUtils();
        JSONArray results = null;
        try {
            final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            final Date date = formatter.parse(formData.getFromDate());
            final Timestamp fromDT = new Timestamp(date.getTime());
            final Date date2 = formatter.parse(formData.getToDate());
            final Timestamp toDT = new Timestamp(date2.getTime());
            results = db.getTable("call AGUNIVERSE.RPT_EDICUSTSTATUSCOUNTBYDATE(?, ?)", fromDT, toDT);
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        catch (ParseException e2) {
            e2.printStackTrace();
        }
        return (ResponseEntity<List<Object>>)new ResponseEntity((Object)results.toList(), HttpStatus.OK);
    }
    
    @PostMapping(path = { "RPT_VENDOREDIBYDATEBYVENDOR" }, consumes = { "application/x-www-form-urlencoded", "multipart/form-data" }, produces = { "application/json" })
    @ResponseBody
    public ResponseEntity<List<Object>> RPT_VENDOREDIBYDATEBYVENDOR(final DTFormData formData) {
        final DBUtils db = new DBUtils();
        JSONArray results = null;
        try {
            final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            final Date date = formatter.parse(formData.getFromDate());
            final Timestamp fromDT = new Timestamp(date.getTime());
            final Date date2 = formatter.parse(formData.getToDate());
            final Timestamp toDT = new Timestamp(date2.getTime());
            results = db.getTable("CALL AGUNIVERSE.RPT_VENDOREDIBYDATEBYVENDOR(?,?)", fromDT, toDT);
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        catch (ParseException e2) {
            e2.printStackTrace();
        }
        return (ResponseEntity<List<Object>>)new ResponseEntity((Object)results.toList(), HttpStatus.OK);
    }
    
    @PostMapping(path = { "RPT_VENDOREDIMATCHBYDATEBYVENDOR" }, consumes = { "application/x-www-form-urlencoded", "multipart/form-data" }, produces = { "application/json" })
    @ResponseBody
    public ResponseEntity<List<Object>> RPT_VENDOREDIMATCHBYDATEBYVENDOR(final DTFormData formData) {
        final DBUtils db = new DBUtils();
        JSONArray results = null;
        try {
            final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            final Date date = formatter.parse(formData.getFromDate());
            final Timestamp fromDT = new Timestamp(date.getTime());
            final Date date2 = formatter.parse(formData.getToDate());
            final Timestamp toDT = new Timestamp(date2.getTime());
            results = db.getTable("CALL AGUNIVERSE.RPT_VENDOREDIMATCHBYDATEBYVENDOR(?,?)", fromDT, toDT);
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        catch (ParseException e2) {
            e2.printStackTrace();
        }
        return (ResponseEntity<List<Object>>)new ResponseEntity((Object)results.toList(), HttpStatus.OK);
    }
    
    @PostMapping(path = { "SP_INSERTSELECTEDITRANSENDBYHOUSE" }, consumes = { "application/x-www-form-urlencoded", "multipart/form-data" }, produces = { "application/json" })
    @ResponseBody
    public ResponseEntity<List<Object>> SP_INSERTSELECTEDITRANSENDBYHOUSE(final DTFormData formData) {
        final DBUtils db = new DBUtils();
        JSONArray results = null;
        try {
            final Integer ediid = formData.getEdiId();
            final String handling = formData.getHandling();
            final Integer house = formData.getHouse();
            final Integer suffix = formData.getSuffix();
            final String rebill = formData.getRebillYN();
            results = db.getTable("CALL AGUNIVERSE.SP_INSERTSELECTEDITRANSENDBYHOUSE(?,?,?,?,?)", (int)ediid, handling, (int)house, (int)suffix, rebill);
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return (ResponseEntity<List<Object>>)new ResponseEntity((Object)results.toList(), HttpStatus.OK);
    }
    
    @PostMapping(path = { "SP_SELECTVENDOR214SUMMARYBYDATEBYSCAC" }, consumes = { "application/x-www-form-urlencoded", "multipart/form-data" }, produces = { "application/json" })
    @ResponseBody
    public ResponseEntity<List<Object>> SP_SELECTVENDOR214SUMMARYBYDATEBYSCAC(final DTFormData formData) {
        final DBUtils db = new DBUtils();
        JSONArray results = null;
        try {
            final Integer vendor = formData.getVendor();
            final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            final Date date = formatter.parse(formData.getFromDate());
            final Timestamp fromDT = new Timestamp(date.getTime());
            final Date date2 = formatter.parse(formData.getToDate());
            final Timestamp toDT = new Timestamp(date2.getTime());
            results = db.getTable("CALL AGUNIVERSE.SP_SELECTVENDOR214SUMMARYBYDATEBYSCAC(?,?,?)", vendor, fromDT, toDT);
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        catch (ParseException e2) {
            e2.printStackTrace();
        }
        return (ResponseEntity<List<Object>>)new ResponseEntity((Object)results.toList(), HttpStatus.OK);
    }
}
