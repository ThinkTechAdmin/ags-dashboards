package com.agsystems.dashboard;

public class InvoiceStatus
{
    private String edid;
    private String invoice;
    private String status;
    
    public InvoiceStatus(final String edid, final String invoice, final String status) {
        this.edid = edid;
        this.invoice = invoice;
        this.status = status;
    }
    
    public InvoiceStatus() {
    }
    
    public String getEdid() {
        return this.edid;
    }
    
    public void setEdid(final String edid) {
        this.edid = edid;
    }
    
    public String getInvoice() {
        return this.invoice;
    }
    
    public void setInvoice(final String invoice) {
        this.invoice = invoice;
    }
    
    public String getStatus() {
        return this.status;
    }
    
    public void setStatus(final String status) {
        this.status = status;
    }
    
    @Override
    public String toString() {
        return String.format("InvoiceStatus[edid='%s', invoice='%s', status='%s']", this.edid, this.invoice, this.status);
    }
}
