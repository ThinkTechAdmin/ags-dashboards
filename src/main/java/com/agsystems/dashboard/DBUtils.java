package com.agsystems.dashboard;

import java.sql.ResultSetMetaData;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.json.JSONObject;
import java.sql.DriverManager;
import org.json.JSONArray;
import java.sql.Timestamp;
import java.sql.Connection;

public class DBUtils
{
    public String jdbcURL;
    public String user;
    public String pass;
    public Connection connection;
    
    public DBUtils(final String uri, final String user, final String pass) {
        this.jdbcURL = uri;
        this.user = user;
        this.pass = pass;
    }
    
    public DBUtils() {
    }
    
    public String getUri() {
        return this.jdbcURL;
    }
    
    public void setUri(final String uri) {
        this.jdbcURL = uri;
    }
    
    public String getUser() {
        return this.user;
    }
    
    public void setUser(final String user) {
        this.user = user;
    }
    
    public String getPass() {
        return this.pass;
    }
    
    public void setPass(final String pass) {
        this.pass = pass;
    }
    
    public Connection getConnection() {
        return this.connection;
    }
    
    public void setConnection(final Connection connection) {
        this.connection = connection;
    }
    
    public JSONArray getTable(final String sql, final Timestamp fromDT, final Timestamp toDT) throws SQLException {
        final JSONArray array = new JSONArray();
        Connection connection = null;
        try {
            Class.forName("com.ibm.as400.access.AS400JDBCDriver");
            connection = DriverManager.getConnection("jdbc:as400://viper.agsystems.com;", "UPLOADDOC", "UPLOAD@23");
            connection.setAutoCommit(false);
            ResultSet rs = null;
            System.out.println("**** Calling SQL " + sql);
            final CallableStatement stmt = connection.prepareCall(sql);
            stmt.setTimestamp(1, fromDT);
            stmt.setTimestamp(2, toDT);
            final boolean results = stmt.execute();
            if (results) {
                rs = stmt.getResultSet();
                final ResultSetMetaData rsmd = rs.getMetaData();
                while (rs.next()) {
                    final JSONObject record = new JSONObject();
                    for (int i = 0; i < rsmd.getColumnCount(); ++i) {
                        record.put(rsmd.getColumnLabel(i + 1), (Object)rs.getString(i + 1));
                    }
                    array.put((Object)record);
                }
            }
            connection.close();
        }
        catch (SQLException ex2) {
            System.out.println("Database error" + ex2.getMessage());
            connection.close();
            ex2.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return array;
    }
    
    public JSONArray getTable(final String sql, final Integer vendor, final Timestamp fromDT, final Timestamp toDT) throws SQLException {
        final JSONArray array = new JSONArray();
        Connection connection = null;
        try {
            Class.forName("com.ibm.as400.access.AS400JDBCDriver");
            connection = DriverManager.getConnection("jdbc:as400://viper.agsystems.com;", "UPLOADDOC", "UPLOAD@23");
            connection.setAutoCommit(false);
            ResultSet rs = null;
            System.out.println("**** Calling SQL " + sql);
            final CallableStatement stmt = connection.prepareCall(sql);
            stmt.setInt(1, vendor);
            stmt.setTimestamp(2, fromDT);
            stmt.setTimestamp(3, toDT);
            final boolean results = stmt.execute();
            if (results) {
                rs = stmt.getResultSet();
                final ResultSetMetaData rsmd = rs.getMetaData();
                while (rs.next()) {
                    final JSONObject record = new JSONObject();
                    for (int i = 0; i < rsmd.getColumnCount(); ++i) {
                        record.put(rsmd.getColumnLabel(i + 1), (Object)rs.getString(i + 1));
                    }
                    array.put((Object)record);
                }
            }
            connection.close();
        }
        catch (SQLException ex2) {
            System.out.println("Database error" + ex2.getMessage());
            connection.close();
            ex2.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return array;
    }
    
    public JSONArray getTable(final String sql, final Timestamp fromDT, final Timestamp toDT, final String Location) throws SQLException {
        final JSONArray array = new JSONArray();
        Connection connection = null;
        try {
            Class.forName("com.ibm.as400.access.AS400JDBCDriver");
            connection = DriverManager.getConnection("jdbc:as400://viper.agsystems.com;", "UPLOADDOC", "UPLOAD@23");
            connection.setAutoCommit(false);
            ResultSet rs = null;
            System.out.println("**** Calling SQL " + sql);
            final CallableStatement stmt = connection.prepareCall(sql);
            stmt.setTimestamp(1, fromDT);
            stmt.setTimestamp(2, toDT);
            stmt.setString(3, Location);
            final boolean results = stmt.execute();
            if (results) {
                rs = stmt.getResultSet();
                final ResultSetMetaData rsmd = rs.getMetaData();
                while (rs.next()) {
                    final JSONObject record = new JSONObject();
                    for (int i = 0; i < rsmd.getColumnCount(); ++i) {
                        record.put(rsmd.getColumnLabel(i + 1), (Object)rs.getString(i + 1));
                    }
                    array.put((Object)record);
                }
            }
            connection.close();
        }
        catch (SQLException ex2) {
            System.out.println("Database error" + ex2.getMessage());
            connection.close();
            ex2.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return array;
    }
    
    public JSONArray getTable(final String sql, final int fromDT, final int toDT, final String Location) throws SQLException {
        final JSONArray array = new JSONArray();
        Connection connection = null;
        try {
            Class.forName("com.ibm.as400.access.AS400JDBCDriver");
            connection = DriverManager.getConnection("jdbc:as400://viper.agsystems.com;", "UPLOADDOC", "UPLOAD@23");
            connection.setAutoCommit(false);
            ResultSet rs = null;
            System.out.println("**** Calling SQL " + sql);
            final CallableStatement stmt = connection.prepareCall(sql);
            stmt.setInt(1, fromDT);
            stmt.setInt(2, toDT);
            stmt.setString(3, Location);
            final boolean results = stmt.execute();
            if (results) {
                rs = stmt.getResultSet();
                final ResultSetMetaData rsmd = rs.getMetaData();
                while (rs.next()) {
                    final JSONObject record = new JSONObject();
                    for (int i = 0; i < rsmd.getColumnCount(); ++i) {
                        record.put(rsmd.getColumnLabel(i + 1), (Object)rs.getString(i + 1));
                    }
                    array.put((Object)record);
                }
            }
            connection.close();
        }
        catch (SQLException ex2) {
            System.out.println("Database error" + ex2.getMessage());
            connection.close();
            ex2.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return array;
    }
    
    public JSONArray getTable(final String sql, final String loc) throws SQLException {
        final JSONArray array = new JSONArray();
        Connection connection = null;
        try {
            Class.forName("com.ibm.as400.access.AS400JDBCDriver");
            connection = DriverManager.getConnection("jdbc:as400://viper.agsystems.com;", "UPLOADDOC", "UPLOAD@23");
            connection.setAutoCommit(false);
            ResultSet rs = null;
            System.out.println("**** Calling SQL " + sql);
            final CallableStatement stmt = connection.prepareCall(sql);
            stmt.setString(1, loc);
            final boolean results = stmt.execute();
            if (results) {
                rs = stmt.getResultSet();
                final ResultSetMetaData rsmd = rs.getMetaData();
                while (rs.next()) {
                    final JSONObject record = new JSONObject();
                    for (int i = 0; i < rsmd.getColumnCount(); ++i) {
                        record.put(rsmd.getColumnLabel(i + 1), (Object)rs.getString(i + 1));
                    }
                    array.put((Object)record);
                }
            }
            connection.close();
        }
        catch (SQLException ex2) {
            System.out.println("Database error" + ex2.getMessage());
            connection.close();
            ex2.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return array;
    }
    
    public JSONArray getTable(final String sql, final int ediID) throws SQLException {
        final JSONArray array = new JSONArray();
        Connection connection = null;
        try {
            Class.forName("com.ibm.as400.access.AS400JDBCDriver");
            connection = DriverManager.getConnection("jdbc:as400://viper.agsystems.com;", "UPLOADDOC", "UPLOAD@23");
            connection.setAutoCommit(false);
            ResultSet rs = null;
            System.out.println("**** Calling SQL " + sql);
            final CallableStatement stmt = connection.prepareCall(sql);
            stmt.setInt(1, ediID);
            final boolean results = stmt.execute();
            if (results) {
                rs = stmt.getResultSet();
                final ResultSetMetaData rsmd = rs.getMetaData();
                while (rs.next()) {
                    final JSONObject record = new JSONObject();
                    for (int i = 0; i < rsmd.getColumnCount(); ++i) {
                        record.put(rsmd.getColumnLabel(i + 1), (Object)rs.getString(i + 1));
                    }
                    array.put((Object)record);
                }
            }
            connection.close();
        }
        catch (SQLException ex2) {
            System.out.println("Database error" + ex2.getMessage());
            connection.close();
            ex2.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return array;
    }
    
    public JSONArray getTable(final String sql) throws SQLException {
        final JSONArray array = new JSONArray();
        Connection connection = null;
        try {
            Class.forName("com.ibm.as400.access.AS400JDBCDriver");
            connection = DriverManager.getConnection("jdbc:as400://viper.agsystems.com;", "UPLOADDOC", "UPLOAD@23");
            connection.setAutoCommit(false);
            ResultSet rs = null;
            System.out.println("**** Calling SQL " + sql);
            final CallableStatement stmt = connection.prepareCall(sql);
            final boolean results = stmt.execute();
            if (results) {
                rs = stmt.getResultSet();
                final ResultSetMetaData rsmd = rs.getMetaData();
                while (rs.next()) {
                    final JSONObject record = new JSONObject();
                    for (int i = 0; i < rsmd.getColumnCount(); ++i) {
                        record.put(rsmd.getColumnLabel(i + 1), (Object)rs.getString(i + 1));
                    }
                    array.put((Object)record);
                }
            }
            connection.close();
        }
        catch (SQLException ex2) {
            System.out.println("Database error" + ex2.getMessage());
            connection.close();
            ex2.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return array;
    }
    
    public JSONArray getTable(final String sql, final int fromDT, final int toDT, final String service, final String loc) throws SQLException {
        final JSONArray array = new JSONArray();
        Connection connection = null;
        try {
            Class.forName("com.ibm.as400.access.AS400JDBCDriver");
            connection = DriverManager.getConnection("jdbc:as400://viper.agsystems.com;", "UPLOADDOC", "UPLOAD@23");
            connection.setAutoCommit(false);
            ResultSet rs = null;
            System.out.println("**** Calling SQL " + sql);
            final CallableStatement stmt = connection.prepareCall(sql);
            stmt.setInt(1, fromDT);
            stmt.setInt(2, toDT);
            stmt.setString(3, service);
            stmt.setString(4, loc);
            final boolean results = stmt.execute();
            if (results) {
                rs = stmt.getResultSet();
                final ResultSetMetaData rsmd = rs.getMetaData();
                while (rs.next()) {
                    final JSONObject record = new JSONObject();
                    for (int i = 0; i < rsmd.getColumnCount(); ++i) {
                        record.put(rsmd.getColumnLabel(i + 1), (Object)rs.getString(i + 1));
                    }
                    array.put((Object)record);
                }
            }
            connection.close();
        }
        catch (SQLException ex2) {
            System.out.println("Database error" + ex2.getMessage());
            connection.close();
            ex2.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return array;
    }
    
    public JSONArray getTable(final String sql, final int edi, final String hast, final int hawb, final int suffix, final String rebill) throws SQLException {
        final JSONArray array = new JSONArray();
        Connection connection = null;
        try {
            Class.forName("com.ibm.as400.access.AS400JDBCDriver");
            connection = DriverManager.getConnection("jdbc:as400://viper.agsystems.com;", "UPLOADDOC", "UPLOAD@23");
            connection.setAutoCommit(false);
            ResultSet rs = null;
            System.out.println("**** Calling SQL " + sql);
            final CallableStatement stmt = connection.prepareCall(sql);
            stmt.setInt(1, edi);
            stmt.setString(2, hast);
            stmt.setInt(3, hawb);
            stmt.setInt(4, suffix);
            stmt.setString(5, rebill);
            final boolean results = stmt.execute();
            if (results) {
                rs = stmt.getResultSet();
                final ResultSetMetaData rsmd = rs.getMetaData();
                while (rs.next()) {
                    final JSONObject record = new JSONObject();
                    for (int i = 0; i < rsmd.getColumnCount(); ++i) {
                        record.put(rsmd.getColumnLabel(i + 1), (Object)rs.getString(i + 1));
                    }
                    array.put((Object)record);
                }
            }
            connection.close();
        }
        catch (SQLException ex2) {
            System.out.println("Database error" + ex2.getMessage());
            connection.close();
            ex2.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return array;
    }
}
